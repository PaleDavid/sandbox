
/*jslint regexp: true, nomen: true, browser: true, continue: true, unparam: true, sloppy: true, eqeq: true, sub: true, vars: true, white: true, forin: true, newcap: true, plusplus: true, maxerr: 50, indent: 4 */

/*global $,jQuery,track,alert,confirm,prompt,console,escape,unescape,PRO,PASS,L,O,M,V,F,S,CMS,PLAY,HELP,Markdown,JsonServer */

$(document).ready(function(){

	
	(function(){
		
		// changed to anonymous function while tinkering branch has changed and broken this into separate lines
		$("#output").html("Hello everyone. Please start adding/staging/committing to this git repo. Changed string.");
		
	}());
	
});